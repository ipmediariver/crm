<?php
/*********************************************************************************
 * The contents of this file are subject to the EspoCRM Advanced
 * Agreement ("License") which can be viewed at
 * http://www.espocrm.com/advanced-pack-agreement.
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * sublicense, resell, rent, lease, distribute, or otherwise  transfer rights
 * or usage to the software.
 * 
 * License ID: cc7e95e77a6ae1546286d69af5ad5a5f
 ***********************************************************************************/

namespace Espo\Modules\Advanced\Hooks\Integration;

use Espo\ORM\Entity;

class MailChimp extends \Espo\Core\Hooks\Base
{
    public static $order = 20;

    protected function init()
    {
        $this->dependencies[] = 'metadata';
    }

    protected function getMetadata()
    {
        return $this->getInjection('metadata');
    }

    public function afterSave(Entity $entity)
    {
        if ($entity->id != 'MailChimp' || $entity->getFetched('enabled') == $entity->get('enabled')) {
            return;
        }

        $metadata = $this->getMetadata();
        $data = array(
            'mailChimpNotification' => array(
                'disabled' => ! ((bool) $entity->get('enabled')),
            ),
        );

        $metadata->set('app', 'popupNotifications', $data);
        
        $metadata->save();
    }
}

