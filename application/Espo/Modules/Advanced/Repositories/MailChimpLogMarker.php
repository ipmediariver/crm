<?php
/*********************************************************************************
 * The contents of this file are subject to the EspoCRM Advanced
 * Agreement ("License") which can be viewed at
 * http://www.espocrm.com/advanced-pack-agreement.
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * sublicense, resell, rent, lease, distribute, or otherwise  transfer rights
 * or usage to the software.
 * 
 * License ID: cc7e95e77a6ae1546286d69af5ad5a5f
 ***********************************************************************************/

namespace Espo\Modules\Advanced\Repositories;

use Espo\ORM\Entity;

class MailChimpLogMarker extends \Espo\Core\ORM\Repositories\RDB
{
    protected $allowedTypes = array('Sent', 'Hard Bounced', 'Soft Bounced', 'Opted Out', 'MemberActivity');
    
    public function findMarker($campaignId, $markerType, $createIfEmpty = true)
    {
        if (in_array($markerType, $this->allowedTypes)) {
            $marker = $this->where(array(
                    'mcCampaignId' => $campaignId,
                    'type' => $markerType
                ))->findOne();
            
            if (empty($marker)) {
                if (!$createIfEmpty) {
                    return false;
                }
                $marker = $this->getEntityManager()->getEntity("MailChimpLogMarker");
                $marker->set('mcCampaignId', $campaignId);
                $marker->set('type', $markerType);
                $this->getEntityManager()->saveEntity($marker);
            }
            
            return $marker;
        }
    }
    
    public function resetMarkers($campaignId)
    {
        foreach ($this->allowedTypes as $type) {
            $marker = $this->findMarker($campaignId, $type, false);
            if (!empty($marker)) {
                $this->getEntityManager()->removeEntity($marker);
            }
        }
    }
}

