<?php
/*********************************************************************************
 * The contents of this file are subject to the EspoCRM Advanced
 * Agreement ("License") which can be viewed at
 * http://www.espocrm.com/advanced-pack-agreement.
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * sublicense, resell, rent, lease, distribute, or otherwise  transfer rights
 * or usage to the software.
 * 
 * License ID: cc7e95e77a6ae1546286d69af5ad5a5f
 ***********************************************************************************/

namespace Espo\Modules\Advanced\Core\Workflow\Actions;

use Espo\Core\Exceptions\Error;
use Espo\Modules\Advanced\Core\Workflow\Utils;

use Espo\ORM\Entity;

class TriggerWorkflow extends Base
{
    /**
     * Main run method
     *
     * @param  array $actionData
     * @return string
     */
    protected function run(Entity $entity, array $actionData)
    {
        $jobData = array(
            'workflowId' => $this->getWorkflowId(),
            'entityId' => $this->getEntity()->get('id'),
            'entityType' => $this->getEntity()->getEntityType(),
            'nextWorkflowId' => $actionData['workflowId'],
            'values' => $entity->getValues()
        );

        $executeTime = null;
        if ($actionData['execution']['type'] != 'immediately') {
            $executeTime = $this->getExecuteTime($actionData);
        }

        if ($executeTime) {
            $job = $this->getEntityManager()->getEntity('Job');

            $job->set(array(
                'serviceName' => 'Workflow',
                'method' => 'jobTriggerWorkflow',
                'data' => json_encode($jobData),
                'executeTime' => $executeTime,
            ));

            $this->getEntityManager()->saveEntity($job);
        } else {
            $service = $this->getServiceFactory()->create('Workflow');
            $service->triggerWorkflow($entity, $actionData['workflowId']);
        }

        return true;
    }


    /**
     * Get email address defined in workflow
     *
     * @param  string $type
     * @return array
     */
    protected function getEmailAddress($type = 'to', $returns = null)
    {
        $data = $this->getActionData();
        $fieldValue = $data[$type];

        switch ($fieldValue) {
            case 'specifiedEmailAddress':
                $emailAddress = array('email' => $data[$type . 'Email']);
                break;

            case 'teamUsers':
            case 'followers':
                $entity = $this->getEntity();

                $emailAddress = array(
                    'entityName' => $entity->getEntityType(),
                    'entityId' => $entity->get('id'),
                    'type' => $fieldValue,
                );
                break;

            case 'specifiedTeams':
                $emailAddress = array(
                    'type' => $fieldValue,
                    'teamsIds' => $data['toSpecifiedTeamsIds'],
                );
                break;

            case 'currentUser':
                $emailAddress = array(
                    'entityName' => $this->getContainer()->get('user')->getEntityType(),
                    'entityId' => $this->getContainer()->get('user')->get('id'),
                );
                break;

            default:
                $fieldEntity = Utils::getFieldValue($this->getEntity(), $fieldValue, true, $this->getEntityManager());
                if ($fieldEntity instanceof \Espo\ORM\Entity) {
                    $emailAddress = array(
                        'entityName' => $fieldEntity->getEntityType(),
                        'entityId' => $fieldEntity->get('id'),
                    );
                }
                break;
        }

        return (isset($emailAddress)) ? $emailAddress : $returns;
    }
}