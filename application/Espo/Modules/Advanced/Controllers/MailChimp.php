<?php
/*********************************************************************************
 * The contents of this file are subject to the EspoCRM Advanced
 * Agreement ("License") which can be viewed at
 * http://www.espocrm.com/advanced-pack-agreement.
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * sublicense, resell, rent, lease, distribute, or otherwise  transfer rights
 * or usage to the software.
 * 
 * License ID: cc7e95e77a6ae1546286d69af5ad5a5f
 ***********************************************************************************/

namespace Espo\Modules\Advanced\Controllers;

use \Espo\Core\Exceptions\BadRequest;

class MailChimp extends \Espo\Core\Controllers\Record
{

    public function actionRead($params)
    {
        
        if (!$this->getAcl()->check('MailChimp') || !$this->getAcl()->check('Campaign', 'read') || !$this->getAcl()->check('TargetList', 'read')) {
            throw new Forbidden();
        }
        $id = $params['id'];
        return $this->getService('MailChimp')->loadRelations($id);
    }
    
    public function actionUpdate($params, $data)
    {
        if (!$this->getAcl()->check('MailChimp')) {
            throw new Forbidden();
        }
        return $this->getService('MailChimp')->saveRelation($params, $data);
    }
    
    public function actionScheduleSync($params)
    {
        if (!$this->getAcl()->check('MailChimp', 'read')) {
            throw new Forbidden();
        }
        $entity = $params['entity'];
        $id = $params['id'];
        return $this->getRecordService()->scheduleSync($entity, $id);
    }
    
    public function actionCheckSynchronization($params, $data)
    {
        return $this->getEntityManager()->getRepository('MailChimp')->checkManualSyncs();
    }
    
}
