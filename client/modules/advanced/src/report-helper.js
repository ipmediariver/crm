/*********************************************************************************
 * The contents of this file are subject to the EspoCRM Advanced
 * Agreement ("License") which can be viewed at
 * http://www.espocrm.com/advanced-pack-agreement.
 * By installing or using this file, You have unconditionally agreed to the
 * terms and conditions of the License, and You may not use this file except in
 * compliance with the License.  Under the terms of the license, You shall not,
 * sublicense, resell, rent, lease, distribute, or otherwise  transfer rights
 * or usage to the software.
 * 
 * License ID: cc7e95e77a6ae1546286d69af5ad5a5f
 ***********************************************************************************/

Espo.define('Advanced:ReportHelper', ['View'], function (Fake) {

    var ReportHelper = function (metadata, language) {
        this.metadata = metadata;
        this.language = language;
    }

    _.extend(ReportHelper.prototype, {

        formatColumn: function (value, result) {
            if (value in result.columnNameMap) {
                return result.columnNameMap[value];
            }
            return value;
        },

        formatGroup: function (gr, value, result) {
            var entityType = result.entityType;

            if (gr in result.groupNameMap) {
                var value = result.groupNameMap[gr][value] || value;
                if (value === null || value == '') {
                    value = this.language.translate('-Empty-', 'labels', 'Report');
                }
                return value;
            }

            if (~gr.indexOf('MONTH:')) {
                return moment(value + '-01').format('MMM YYYY');
            } else if (~gr.indexOf('DAY:')) {
                return moment(value).format('MMM DD');
            }

            if (value === null || value == '') {
                return this.language.translate('-Empty-', 'labels', 'Report');
            }
            return value;
        },

        getCode: function () {
            return 'cc7e95e77a6ae1546286d69af5ad5a5f';
        }

    });

    return ReportHelper;

});
