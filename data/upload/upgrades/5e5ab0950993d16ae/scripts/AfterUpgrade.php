<?php
/************************************************************************
 * This file is part of EspoCRM.
 *
 * EspoCRM - Open Source CRM application.
 * Copyright (C) 2014  Yuri Kuznetsov, Taras Machyshyn, Oleksiy Avramenko
 * Website: http://www.espocrm.com
 *
 * EspoCRM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * EspoCRM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EspoCRM. If not, see http://www.gnu.org/licenses/.
 ************************************************************************/

class AfterUpgrade
{
    public function run($container)
    {
        $entityManager = $container->get('entityManager');

        $job = $entityManager->getEntity('Job');
        $dt = new \DateTime();
        $dt->modify('+10 minutes');
        $job->set([
            'serviceName' => 'App',
            'methodName' => 'jobPopulateOptedOutPhoneNumbers',
            'executeTime' => $dt->format('Y-m-d H:i:s')
        ]);
        $entityManager->saveEntity($job);

        $fileManager = $container->get('fileManager');
        $fileManager->removeInDir('vendor/phpoffice/phpexcel', true);

        $this->container = $container;
        $this->renameIndexes();
    }

    protected function renameIndexes()
    {
        $toRebuild = false;

        $toRebuild = $this->renameIndex('email', 'Email', 'dateSent', ['dateSent', 'deleted'], 'IDX_DATE_SENT') || $toRebuild;
        $toRebuild = $this->renameIndex('email', 'Email', 'createdById', ['createdById'], 'IDX_CREATED_BY_ID') || $toRebuild;

        if ($toRebuild) {
            $this->container->get('dataManager')->rebuild();
        }
    }

    protected function renameIndex($tableName, $entityType, $name, $columnList, $to)
    {
        $entityManager = $this->container->get('entityManager');

        $from = $this->getActualIndexName($entityType, $columnList);

        if ($from === $to) return;

        $sql = "SHOW INDEX FROM `{$tableName}` WHERE KEY_NAME = '{$from}'";
        $pdo = $entityManager->getPDO();
        $sth = $pdo->prepare($sql);
        $sth->execute();
        if ($sth->fetchAll()) {
            try {
                $sql = "ALTER TABLE `{$tableName}` RENAME INDEX {$from} TO {$to}";
                $pdo->query($sql);
            } catch (\Exception $e) {
                $sql = "DROP INDEX {$from} ON `{$tableName}`";
                $pdo->query($sql);
                return true;
            }
        }
    }

    protected function getActualIndexName($entityType, array $columnList)
    {
        $databaseIndexes = $this->getActualIndexes();

        $tableName = \Espo\Core\Utils\Util::toUnderScore($entityType);
        $indexColumns = \Espo\Core\Utils\Util::toUnderScore($columnList);

        if (isset($databaseIndexes[$tableName])) {
            return $this->getIndexKeyByColumns($indexColumns, $databaseIndexes[$tableName]);
        }
    }

    protected function getActualIndexes()
    {
        $databaseHelper = new \Espo\Core\Utils\Database\Helper($this->container->get('config'));
        $databaseSchema = $databaseHelper->getDbalConnection()->getSchemaManager()->createSchema();

        $indexes = [];
        foreach ($databaseSchema->getTables() as $table) {
            $tableName = $table->getName();
            foreach ($table->getIndexes() as $indexName => $index) {
                $indexKey = strtoupper($indexName);
                $indexes[$tableName][$indexKey]['columns'] = $index->getColumns();
            }
        }

        return $indexes;
    }

    protected function getIndexKeyByColumns(array $columns, array $tableIndexes)
    {
        foreach ($tableIndexes as $indexKey => $indexData) {
            if ($columns === $indexData['columns']) {
                return $indexKey;
            }
        }
    }
}
