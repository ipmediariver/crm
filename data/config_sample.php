<?php
return [
    'cacheTimestamp' => 1594599848,
    'database' => [
        'driver' => 'pdo_mysql',
        'dbname' => 'crm',
        'user' => 'root',
        'password' => '',
        'port' => '',
        'host' => '127.0.0.1'
    ],
    'useCache' => true,
    'recordsPerPage' => 20,
    'recordsPerPageSmall' => 5,
    'applicationName' => 'Dynamic Communications CRM',
    'version' => '5.8.2',
    'timeZone' => 'America/Tijuana',
    'dateFormat' => 'MM/DD/YYYY',
    'timeFormat' => 'hh:mm a',
    'weekStart' => 1,
    'thousandSeparator' => ',',
    'decimalMark' => '.',
    'exportDelimiter' => ';',
    'currencyList' => [
        0 => 'USD'
    ],
    'defaultCurrency' => 'USD',
    'baseCurrency' => 'USD',
    'currencyRates' => [
        
    ],
    'outboundEmailIsShared' => true,
    'outboundEmailFromName' => 'EspoCRM',
    'outboundEmailFromAddress' => 'dynacomcrm@godynacom.com',
    'smtpServer' => 'smtp.office365.com',
    'smtpPort' => 587,
    'smtpAuth' => true,
    'smtpSecurity' => 'TLS',
    'smtpUsername' => 'dynacomcrm@godynacom.com',
    'smtpPassword' => 'R3port3s357!',
    'languageList' => [
        0 => 'en_GB',
        1 => 'en_US',
        2 => 'es_MX',
        3 => 'cs_CZ',
        4 => 'da_DK',
        5 => 'de_DE',
        6 => 'es_ES',
        7 => 'fr_FR',
        8 => 'id_ID',
        9 => 'it_IT',
        10 => 'nb_NO',
        11 => 'nl_NL',
        12 => 'tr_TR',
        13 => 'sr_RS',
        14 => 'ro_RO',
        15 => 'ru_RU',
        16 => 'pl_PL',
        17 => 'pt_BR',
        18 => 'uk_UA',
        19 => 'vi_VN',
        20 => 'zh_CN',
        21 => 'lt_LT',
        22 => 'hr_HR',
        23 => 'hu_HU',
        24 => 'sk_SK',
        25 => 'fa_IR',
        26 => 'lv_LV'
    ],
    'language' => 'es_MX',
    'logger' => [
        'path' => 'data/logs/espo.log',
        'level' => 'WARNING',
        'rotation' => true,
        'maxFileNumber' => 30
    ],
    'authenticationMethod' => 'Espo',
    'globalSearchEntityList' => [
        0 => 'Account',
        1 => 'Contact',
        2 => 'Lead',
        3 => 'Opportunity'
    ],
    'tabList' => [
        0 => 'Account',
        1 => 'Contact',
        2 => 'Lead',
        3 => 'Opportunity',
        4 => 'Case',
        5 => 'Email',
        6 => 'Calendar',
        7 => 'Meeting',
        8 => 'Call',
        9 => 'Task',
        10 => '_delimiter_',
        11 => 'Document',
        12 => 'Campaign',
        13 => 'KnowledgeBaseArticle',
        14 => 'Stream',
        15 => 'User',
        16 => 'Quote',
        17 => 'Product',
        18 => 'Report',
        19 => 'Soluciones'
    ],
    'quickCreateList' => [
        0 => 'Account',
        1 => 'Contact',
        2 => 'Lead',
        3 => 'Opportunity',
        4 => 'Meeting',
        5 => 'Call',
        6 => 'Task',
        7 => 'Case',
        8 => 'Email'
    ],
    'exportDisabled' => false,
    'assignmentEmailNotifications' => true,
    'assignmentEmailNotificationsEntityList' => [
        0 => 'Lead',
        1 => 'Opportunity',
        2 => 'Task',
        3 => 'Case',
        4 => 'Call'
    ],
    'assignmentNotificationsEntityList' => [
        0 => 'Meeting',
        1 => 'Call',
        2 => 'Email'
    ],
    'portalStreamEmailNotifications' => true,
    'streamEmailNotificationsEntityList' => [
        0 => 'Case'
    ],
    'emailMessageMaxSize' => 10,
    'notificationsCheckInterval' => 10,
    'disabledCountQueryEntityList' => [
        0 => 'Email'
    ],
    'maxEmailAccountCount' => 2,
    'followCreatedEntities' => false,
    'b2cMode' => false,
    'restrictedMode' => false,
    'theme' => 'HazyblueVertical',
    'massEmailMaxPerHourCount' => 100,
    'personalEmailMaxPortionSize' => 10,
    'inboundEmailMaxPortionSize' => 20,
    'authTokenLifetime' => 0,
    'authTokenMaxIdleTime' => 120,
    'userNameRegularExpression' => '[^a-z0-9\\-@_\\.\\s]',
    'addressFormat' => 1,
    'displayListViewRecordCount' => true,
    'dashboardLayout' => [
        0 => (object) [
            'name' => 'My Espo',
            'layout' => [
                0 => (object) [
                    'id' => 'default-activities',
                    'name' => 'Activities',
                    'x' => 2,
                    'y' => 2,
                    'width' => 2,
                    'height' => 2
                ],
                1 => (object) [
                    'id' => 'default-stream',
                    'name' => 'Stream',
                    'x' => 0,
                    'y' => 0,
                    'width' => 2,
                    'height' => 4
                ],
                2 => (object) [
                    'id' => 'default-tasks',
                    'name' => 'Tasks',
                    'x' => 2,
                    'y' => 0,
                    'width' => 2,
                    'height' => 2
                ]
            ]
        ]
    ],
    'calendarEntityList' => [
        0 => 'Meeting',
        1 => 'Call',
        2 => 'Task'
    ],
    'activitiesEntityList' => [
        0 => 'Meeting',
        1 => 'Call'
    ],
    'historyEntityList' => [
        0 => 'Meeting',
        1 => 'Call',
        2 => 'Email'
    ],
    'lastViewedCount' => 20,
    'cleanupJobPeriod' => '1 month',
    'cleanupActionHistoryPeriod' => '15 days',
    'cleanupAuthTokenPeriod' => '1 month',
    'currencyFormat' => 1,
    'currencyDecimalPlaces' => NULL,
    'aclStrictMode' => false,
    'isInstalled' => true,
    'siteUrl' => 'http://suite-crm.dynacomus.com',
    'passwordSalt' => 'b949f992356fa534',
    'cryptKey' => '745f0aa1f09fbf04aa3eeaaf8f631de1',
    'defaultPermissions' => [
        'user' => 48,
        'group' => 48
    ],
    'companyLogoId' => '5e5ab874e83299cb2',
    'companyLogoName' => 'LOGO_WHITE_HORIZONTAL-01.png',
    'avatarsDisabled' => false,
    'userThemesDisabled' => false,
    'dashletsOptions' => (object) [
        
    ],
    'massEmailDisableMandatoryOptOutLink' => false,
    'mentionEmailNotifications' => true,
    'streamEmailNotifications' => true,
    'notificationSoundsDisabled' => false,
    'adminNotifications' => true,
    'adminNotificationsNewVersion' => true,
    'adminNotificationsCronIsNotConfigured' => true,
    'streamEmailNotificationsTypeList' => [
        0 => 'Post',
        1 => 'Status',
        2 => 'EmailReceived'
    ],
    'fullTextSearchMinLength' => 4,
    'massPrintPdfMaxCount' => 50,
    'emailKeepParentTeamsEntityList' => [
        0 => 'Case'
    ],
    'adminNotificationsNewExtensionVersion' => true,
    'recordListMaxSizeLimit' => 1000,
    'noteDeleteThresholdPeriod' => '1 month',
    'noteEditThresholdPeriod' => '7 days',
    'maintenanceMode' => false,
    'cronDisabled' => false,
    'actualDatabaseType' => 'mariadb',
    'actualDatabaseVersion' => '10.4.13',
    'hashSecretKey' => 'b6cfc3de381247faea5a3f60c8aaf0f2',
    'personNameFormat' => 'firstLast',
    'scopeColorsDisabled' => false,
    'tabColorsDisabled' => false,
    'tabIconsDisabled' => false,
    'apiSecretKeys' => (object) [
        
    ],
    'useWebSocket' => false,
    'textFilterUseContainsForVarchar' => false,
    'emailAddressIsOptedOutByDefault' => false,
    'aclAllowDeleteCreated' => false,
    'cleanupDeletedRecords' => false,
    'fiscalYearShift' => 0,
    'addressCountryList' => [
        
    ],
    'addressCityList' => [
        
    ],
    'addressStateList' => [
        
    ]
];
?>